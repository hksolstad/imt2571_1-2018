<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;

    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db)
        {
            $this->db = $db;
        }
        else        // Create PDO connection
        {
            try
            {
                $this->db = new PDO('mysql:host=127.0.0.1;dbname=test;charset=utf8',
                'root','');
                    // Set server in exception mode
                $this->db->setAttribute(PDO::ATTR_ERRMODE,
                                        PDO::ERRMODE_EXCEPTION);
                $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            }
            catch(PDOException $feil)
            {
                throw $feil;
            }

        }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();

        try
        {   //  store results of the query in $stmt
            $stmt = $this->db->query("SELECT id, title, author, description
                                      FROM book
                                      ORDER BY id");
        }
        catch (PDOException $feil)
        {
            throw $feil;
        }
        if ($stmt) {
            foreach ($stmt as $row)
            {
              // for each row in the result store the value to the corespondig column
                array_push($booklist, new Book($row['title'],
                                               $row['author'],
                                               $row['description'],
                                               $row['id']));
            }
        }
        else {
            throw new Execption("feil");
        }
        return $booklist;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        $book = null;

        try
        {       //Throws an exception if the id is not a number
            if (is_numeric($id))
            {   //  store results of the query in $stmt
                $stmt = $this->db->query("SELECT title, author, description, id
                                          FROM book
                                          WHERE id=$id");
                //Checks if there was a result
                if($stmt->rowCount()>0){
                  //  fetch the column names for the results
                  $row = $stmt->fetch(PDO::FETCH_ASSOC);
                  // for each row in the result store the value to the corespondig column
                  $book = new Book($row['title'],
                                   $row['author'],
                                   $row['description'],
                                   $id);
                }
                else
                {
                  throw new InvalidArgumentException('Invalid paramter,
                                                      please use only numbers');
                }

            }
            else
            {
                throw new InvalidArgumentException('Invalid parameter, expected numbers');
            }
        }
        catch (InvalidArgumentException $feil)
        {
              throw $feil;
        }

        return $book;
    }

    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
      try
      {
        //Throws an exception if mandatory fields are empty
        if (empty($book->title) || empty($book->author))
        {
            throw new InvalidArgumentException('Mandatory parameters empty');
        }

        elseif (!is_numeric($book->id))
        {
            throw new InvalidArgumentException('Invalid parameter, expected numbers');
        }
              $stmt = $this->db->prepare("INSERT INTO book (title, author,
                                                            description)
                                          VALUES (:newTitle, :newAuthor,
                                                  :newDescription)");
                  //  bind the value to the parameters
              $stmt->bindValue(':newTitle',$book->title, PDO::PARAM_STR);
              $stmt->bindValue(':newAuthor',$book->author, PDO::PARAM_STR);
              $stmt->bindValue(':newDescription',$book->description, PDO::PARAM_STR);
              $stmt->execute();
              $book->id = $this->db->lastInsertID();
      }
      catch (InvalidArgumentException $feil)
      {
          throw $feil;
      }
    }

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
          try
          {
            // throws an exception if mandatory fields are empty
              if (empty($book->id) || empty($book->title) ||
                    empty($book->author)){
                      throw new InvalidArgumentException('Mandatory parameters empty');
                    }
                      //  throws an exception if is is not a number
              elseif (!is_numeric($book->id))
              {
                  throw new InvalidArgumentException('Invalid parameter, expected numbers');
              }
              else
              {
                  $stmt = $this->db->prepare("UPDATE book
                                              SET title = :newTitle,
                                                  author = :newAuthor,
                                                  description = :newDescription
                                              WHERE id = $book->id");
                  //  bind the value to the parameters
                  $stmt->bindValue(':newTitle',$book->title, PDO::PARAM_STR);
                  $stmt->bindValue(':newAuthor',$book->author, PDO::PARAM_STR);
                  $stmt->bindValue(':newDescription',$book->description, PDO::PARAM_STR);
                  $stmt->execute();
              }
          }
          catch (InvalidArgumentException $feil)
          {
              throw $feil;
          }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {

      try
      {     //  throws an exception if the id is not a number
          if (is_numeric($id))
          {
              $this->db->query("DELETE FROM book WHERE id = $id");
          }
          else
          {
              throw new InvalidArgumentException('Invalid paramter,
                                                  please use only numbers');
          }
      }
      catch (InvalidArgumentException $feil)
      {
            throw $feil;
      }
    }
}
