<?php

require_once('C:/xampp/htdocs/IMT2571/Assignment1/Model/DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;

    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'root',
                '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals($bookList[0]->id, 1);
        $this->assertEquals($bookList[0]->title, 'Jungle Book');
        $this->assertEquals($bookList[1]->id, 2);
        $this->assertEquals($bookList[1]->author, 'J. Walker');
        $this->assertEquals($bookList[2]->id, 3);
        $this->assertEquals($bookList[2]->description, 'Written by some smart gal.');
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);

        // Id was successfully assigned
        $this->assertEquals($book->id, 4);

        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
      $testValues = ['id' => 'haha',
                     'title' => 'New book',
                     'author' => 'Some author',
                     'description' => 'Some description'];
      $book = new Book($testValues['id'], $testValues['title'], $testValues['author'], $testValues['description']);
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->addBook($book);
    }

    // Tests that adding a book fails mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
      $testValues = ['title' => 'New book',
                     'author' => '',
                     'description' => 'Some description'];
      $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->addBook($book);
    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
      //1
      $testValues = ['title' => "New book",
                     'author' => "Some author",
                     'description' => "Some description"];
      $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
      $this->dbModel->modifyBook($book);

      //2
      $testValues = ['title' => "New book",
                     'author' => "Some author",
                     'description' => ""];
      $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
      $this->dbModel->modifyBook($book);

      //3
      $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                     'author' => "<script>document.body.style.visibility='hidden'</script>",
                     'description' => "<script>document.body.style.visibility='hidden'</script>"];
      $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
      $this->dbModel->modifyBook($book);
    }

    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
      $testValues = ['id' => 'haha',
                     'title' => 'New book',
                     'author' => 'Some author',
                     'description' => 'Some description'];
      $book = new Book($testValues['id'], $testValues['title'], $testValues['author'], $testValues['description']);
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->modifyBook($book);
    }

    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {
      $testValues = ['title' => 'New book',
                     'author' => '',
                     'description' => 'Some description'];
      $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->modifyBook($book);
    }

    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
      $this->dbModel->deleteBook(2);
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->getBookById(2);
      //$this->assertNull($this->seeInDatabase(2));
    }

    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
      $this->expectException(InvalidArgumentException::class);
      $this->dbModel->deleteBook("haha");
    }
}
